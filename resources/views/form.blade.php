<!DOCTYPE html>
<html>
<head>
	<title>SanberBook</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="POST">
		@csrf
		<label for="fname">First Name :</label>
		<br>
		<input type="text" name="fname" >
		<br>
		<label  for="lname">Last Name :</label>
		<br>	
		<input type="text" name="lname">
		<br>
		<br>
		<div>Gender
				<br>
				<input type="radio" id="male" name="gender" value="male">
						<label for="male">Male</label><br>
				<input type="radio" id="female" name="gender" value="female">
						<label for="female">Female</label><br>
				<input type="radio" id="other" name="gender" value="other">
						<label for="other">Other</label>
		</div>
		<br>
			<div>	
			<label for="nationality">Nationality</label><br>
			<select id="nationality" name="nationality">
				<option value="Indonesia">Indonesian</option>
				<option value="Singapore">Singapore</option>
				<option value="Malaysia">Malaysia</option>
				<option value="Australia">Australia</option>
			</select>
		</div>
		<br>
		<div>
		<label>Languge Spoken</label>
		<br>
		<input type="checkbox" name="bahasaIndonesia">
				<label for="bahasaIndonesia">Bahasa Indonesia</label>
		<br>	
		<input type="checkbox" name="english">
				<label for="english">english</label>
		<br>	
		<input type="checkbox" name="other">
				<label for="other">Other</label>
		<br>	
		</div>
		<br>			
		<div>
			<label for="bio">Bio</label>
			<br>
			<textarea name="BIo">enter here</textarea>
		</div>
		<div>
		<br>	
		<input type="submit" name="submit" value="submit">
		</div>
	</form>
</body>
</html>