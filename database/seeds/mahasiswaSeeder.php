<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class mahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('id_ID');

    	for ($i=0; $i <50 ; $i++) {
    		DB::table('mahasiswa')->insert
    		([
    			'nama'=>$faker->name,
        		'nim'=>'18080010100'.$faker->numberBetween(10,99),
        		'email' => $faker->email,
        		'jurusan'=>$faker->jobTitle
        	]);
    	}
    }
}
