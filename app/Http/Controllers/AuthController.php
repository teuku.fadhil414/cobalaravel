<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function welcome(Request $request)
    {
    	return view('selamatdatang', ['request'=>$request]);
    }

}
